SET AUTOTRACE ON
set echo on
set time on
set timing on
set serveroutput on

--Paymethods stats
select methodname, amount from(
select p.paymethod_id as mid, sum(p.amount) as amount
from payouts p
group by p.paymethod_id)
left join paymethods m on mid=m.id;
list;

--Unpaid users
select users.firstname AS firstname,users.surname,
paymethods.methodName AS methodName,payouts.amount AS amount,payouts.account AS account,
payouts.requestDate AS requestDate 
from (
users left join payouts on(users.id = payouts.user_id) 
left join paymethods on(payouts.paymethod_id = paymethods.id)) 
where payouts.status = 0 and users.ban=0
order by payouts.requestDate asc;
list;

--Users to contact
select f||' '||s as name, e as email, r as reason from(
  select u.id as id, u.firstname as f, u.surname as s, u.email as e, 'User have some unapproved site' as r 
  from users u where id in(select user_id from sites where approved = 0)
UNION
  select u.id as id, u.firstname as f, u.surname as s, u.email as e, 'User have some unapproved banner' as r 
  from users u where id in(select user_id from banners where approved = 0)
UNION
  select u.id as id, u.firstname as f, u.surname as s, u.email as e, 'User is banned' as r 
  from users u where ban=1
UNION
  select u.id as id, u.firstname as f, u.surname as s, u.email as e, 'User is waiting for payout' as r 
  from users u where id in(select user_id from payouts where status = 0))
order by id;
list;

--Month click stats grouped by banners
select month_number, bn.title as BANNER_TITLE,bnclicks CLICKS  from( 
  select ip.banner_id AS banner_id, EXTRACT(month FROM ip.clicktime) AS month_number, count(*) AS bnclicks 
  from ip 
  where (ip.clicktime >= SYSTIMESTAMP-365) 
  group by EXTRACT(month FROM ip.clicktime),ip.banner_id
  )
  left join banners bn on bn.id=banner_id;
list;
  
-- Users withdrawal stats
SELECT (SELECT firstname||' '||surname as name from users where id=p.user_id) as name,min(p.requestdate) as first_request, 
max (p.requestdate) as last_request, count(*) as count_requests, SUM(p.AMOUNT) as total_amount 
  FROM
  PAYOUTS P
  where p.status = 1
  GROUP BY p.user_id
  ORDER by SUM(p.AMOUNT) desc;
list;  

