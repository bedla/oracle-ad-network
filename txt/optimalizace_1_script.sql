SET AUTOTRACE ON
set echo on
set time on
set timing on
set serveroutput on

--predchozi
select methodname, amount from(
select p.paymethod_id as mid, sum(p.amount) as amount
from payouts p
group by p.paymethod_id)
left join paymethods m on mid=m.id;

--optimalizovany
select (select methodname from paymethods where id=p.paymethod_id), sum(p.amount) as amount
from payouts p
group by p.paymethod_id;
