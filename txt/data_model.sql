--------------------------------------------------------
--  DDL for Table BANNERS
--------------------------------------------------------

  CREATE TABLE "BANNERS" 
   (	"ID" NUMBER(12,0), 
	"APPROVED" NUMBER(1,0), 
	"IMG" BLOB, 
	"URL" VARCHAR2(1024 BYTE), 
	"TITLE" VARCHAR2(50 BYTE), 
	"USER_ID" NUMBER(12,0)
   );
--------------------------------------------------------
--  DDL for Table CREDITS
--------------------------------------------------------

  CREATE TABLE "CREDITS" 
   (	"USER_ID" NUMBER(12,0), 
	"CREDITS" NUMBER(10,4)
   );
--------------------------------------------------------
--  DDL for Table IP
--------------------------------------------------------

  CREATE TABLE "IP" 
   (	"ID" NUMBER(12,0), 
	"BANNER_ID" NUMBER(12,0), 
	"IP" VARCHAR2(45 BYTE), 
	"CLICKTIME" DATE, 
	"SITE_ID" NUMBER(12,0)
   );
--------------------------------------------------------
--  DDL for Table PAYMETHODS
--------------------------------------------------------

  CREATE TABLE "PAYMETHODS" 
   (	"ID" NUMBER(12,0), 
	"METHODNAME" VARCHAR2(20 BYTE)
   );
--------------------------------------------------------
--  DDL for Table PAYOUTS
--------------------------------------------------------

  CREATE TABLE "PAYOUTS" 
   (	"ID" NUMBER(12,0), 
	"USER_ID" NUMBER(12,0), 
	"AMOUNT" NUMBER(10,2), 
	"ACCOUNT" VARCHAR2(255 BYTE), 
	"REQUESTDATE" DATE, 
	"PAYMETHOD_ID" NUMBER(12,0), 
	"STATUS" NUMBER(1,0)
   );
--------------------------------------------------------
--  DDL for Table SITES
--------------------------------------------------------

  CREATE TABLE "SITES" 
   (	"ID" NUMBER(12,0), 
	"APPROVED" NUMBER(1,0), 
	"USER_ID" NUMBER(12,0), 
	"URL" VARCHAR2(1024 BYTE)
   );
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "USERS" 
   (	"ID" NUMBER(12,0), 
	"EMAIL" VARCHAR2(255 BYTE), 
	"PASSWORD" CHAR(64 BYTE), 
	"FIRSTNAME" VARCHAR2(30 BYTE), 
	"REGISTRATIONDATE" DATE DEFAULT sysdate, 
	"BAN" NUMBER(1,0) DEFAULT 0, 
	"SURNAME" VARCHAR2(50 BYTE)
   );