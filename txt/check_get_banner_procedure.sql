DECLARE
  IN_IP VARCHAR2(200);
  IMG BLOB;
  BID NUMBER;
  SITE_ID NUMBER;
BEGIN
  IN_IP := '89.78.56.145';
  IMG := null;
  BID := NULL;
  SITE_ID := 701;
  dbms_output.put_line('RUNNING PROCEDURE GET BANNER, BID SHOULD BE 1 OR 2 OR 3 OR 4 OR 5.');
  BANNER_API.GET_BANNER(
    IN_IP => IN_IP,
    IMG => IMG,
    BID => BID
  );
  --dbms_output.put_line('BID='||BID);
    BANNER_API.BANNERCLICK(
    IN_IP => IN_IP,
    SITE_ID => SITE_ID,
    BANNER_ID => bid
  );
  dbms_output.put_line('CALLED BANNERCLICK, NEXT BID SHOULD NOT BE '||BID);
  BANNER_API.GET_BANNER(
    IN_IP => IN_IP,
    IMG => IMG,
    BID => BID
  );
 
ROLLBACK; 
END;
