SET AUTOTRACE ON
set echo on
set time on
set timing on
set serveroutput on

--predchozi
select f||' '||s as name, e as email, r as reason from(
  select u.id as id, u.firstname as f, u.surname as s, u.email as e, 'User have some unapproved site' as r 
  from users u where id in(select user_id from sites where approved = 0)
UNION
  select u.id as id, u.firstname as f, u.surname as s, u.email as e, 'User have some unapproved banner' as r 
  from users u where id in(select user_id from banners where approved = 0)
UNION
  select u.id as id, u.firstname as f, u.surname as s, u.email as e, 'User is banned' as r 
  from users u where ban=1
UNION
  select u.id as id, u.firstname as f, u.surname as s, u.email as e, 'User is waiting for payout' as r 
  from users u where id in(select user_id from payouts where status = 0))
order by id;
list;

--optimalizovany
select u.firstname||' '||u.surname as name, u.email as email, ids.r as reason 
from
(select user_id as usid, 'User have some unapproved site' as r from sites where approved = 0
UNION
select user_id as usid, 'User have some unapproved banner' as r from banners where approved = 0
UNION
select u.id as usid,'User is banned' as r from users u where ban=1
UNION
select user_id as usid, 'User is waiting for payout' as r  from payouts where status = 0) ids
left join users u on (u.id=ids.usid)
order by uid;