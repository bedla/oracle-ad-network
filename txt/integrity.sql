--------------------------------------------------------
--  DDL for Sequence ADPACKS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ADPACKS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;
--------------------------------------------------------
--  DDL for Sequence BANNERS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "BANNERS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 101 CACHE 20 NOORDER  NOCYCLE;
--------------------------------------------------------
--  DDL for Sequence IP_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "IP_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2581 CACHE 20 NOORDER  NOCYCLE;
--------------------------------------------------------
--  DDL for Sequence PAYMETHODS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "PAYMETHODS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE;
--------------------------------------------------------
--  DDL for Sequence PAYOUTS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "PAYOUTS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1341 CACHE 20 NOORDER  NOCYCLE;
--------------------------------------------------------
--  DDL for Sequence SITES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SITES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2901 CACHE 20 NOORDER  NOCYCLE;
--------------------------------------------------------
--  DDL for Sequence USERS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "USERS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1141 CACHE 20 NOORDER  NOCYCLE;
   
   --------------------------------------------------------
--  DDL for Index EMAIL_UNIQUE
--------------------------------------------------------

  CREATE UNIQUE INDEX "EMAIL_UNIQUE" ON "USERS" ("EMAIL");
--------------------------------------------------------
--  DDL for Index PK_BANNER
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_BANNER" ON "BANNERS" ("ID");
--------------------------------------------------------
--  DDL for Index PK_IP
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_IP" ON "IP" ("ID");
--------------------------------------------------------
--  DDL for Index PK_PAYMETHOD
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_PAYMETHOD" ON "PAYMETHODS" ("ID");
--------------------------------------------------------
--  DDL for Index PK_PAYOUT
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_PAYOUT" ON "PAYOUTS" ("ID");
--------------------------------------------------------
--  DDL for Index PK_SITE
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_SITE" ON "SITES" ("ID");

--------------------------------------------------------
--  DDL for Index USER_ID_UNIQUE
--------------------------------------------------------

  CREATE UNIQUE INDEX "USER_ID_UNIQUE" ON "CREDITS" ("USER_ID");
--------------------------------------------------------
--  DDL for Index USERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USERS_PK" ON "USERS" ("ID");
--------------------------------------------------------
--  Constraints for Table BANNERS
--------------------------------------------------------

  ALTER TABLE "BANNERS" ADD CONSTRAINT "APPROVED_CHECK_BOOLEAN" CHECK (APPROVED=1 OR APPROVED=0) ENABLE;
  ALTER TABLE "BANNERS" ADD CONSTRAINT "PK_BANNER" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "BANNERS" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "BANNERS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CREDITS
--------------------------------------------------------

  ALTER TABLE "CREDITS" ADD CONSTRAINT "USER_ID_UNIQUE" UNIQUE ("USER_ID") ENABLE;
  ALTER TABLE "CREDITS" MODIFY ("USER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table IP
--------------------------------------------------------

  ALTER TABLE "IP" ADD CONSTRAINT "IP_CHECK" CHECK (regexp_like(IP, '^([01]?[[:digit:]][[:digit:]]?|2[0-4][[:digit:]]|25[0-5])\.([01]?[[:digit:]][[:digit:]]?|2[0-4][[:digit:]]|25[0-5])\.([01]?[[:digit:]][[:digit:]]?|2[0-4][[:digit:]]|25[0-5])\.([01]?[[:digit:]][[:digit:]]?|2[0-4][[:digit:]]|25[0-5])$')) ENABLE;
  ALTER TABLE "IP" ADD CONSTRAINT "PK_IP" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "IP" MODIFY ("SITE_ID" NOT NULL ENABLE);
  ALTER TABLE "IP" MODIFY ("CLICKTIME" NOT NULL ENABLE);
  ALTER TABLE "IP" MODIFY ("IP" NOT NULL ENABLE);
  ALTER TABLE "IP" MODIFY ("BANNER_ID" NOT NULL ENABLE);
  ALTER TABLE "IP" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PAYMETHODS
--------------------------------------------------------

  ALTER TABLE "PAYMETHODS" ADD CONSTRAINT "PK_PAYMETHOD" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "PAYMETHODS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PAYOUTS
--------------------------------------------------------

  ALTER TABLE "PAYOUTS" ADD CONSTRAINT "PK_PAYOUT" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "PAYOUTS" MODIFY ("STATUS" NOT NULL ENABLE);
  ALTER TABLE "PAYOUTS" MODIFY ("PAYMETHOD_ID" NOT NULL ENABLE);
  ALTER TABLE "PAYOUTS" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "PAYOUTS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SITES
--------------------------------------------------------

  ALTER TABLE "SITES" ADD CONSTRAINT "PK_SITE" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SITES" MODIFY ("URL" NOT NULL ENABLE);
  ALTER TABLE "SITES" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "SITES" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "SITES" ADD CONSTRAINT "SITE_APPROVED_CHECK_BOOLEAN" CHECK (APPROVED = 0 OR APPROVED = 1) ENABLE;
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "USERS" ADD CONSTRAINT "EMAIL_UNIQUE" UNIQUE ("EMAIL") ENABLE;
  ALTER TABLE "USERS" ADD CONSTRAINT "CHECK_BAN_BOOLEAN" CHECK (BAN=0 OR BAN=1) ENABLE;
  ALTER TABLE "USERS" ADD CONSTRAINT "EMAIL_CHECK" CHECK (regexp_like(EMAIL, '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')) ENABLE;
  ALTER TABLE "USERS" ADD CONSTRAINT "USERS_PK" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "USERS" MODIFY ("REGISTRATIONDATE" NOT NULL ENABLE);
  ALTER TABLE "USERS" MODIFY ("PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "USERS" MODIFY ("EMAIL" NOT NULL ENABLE);
  ALTER TABLE "USERS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table BANNERS
--------------------------------------------------------

  ALTER TABLE "BANNERS" ADD CONSTRAINT "FK_BANNERS_USER_ID" FOREIGN KEY ("USER_ID")
	  REFERENCES "USERS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CREDITS
--------------------------------------------------------

  ALTER TABLE "CREDITS" ADD CONSTRAINT "FK_CREDITS_USER_ID" FOREIGN KEY ("USER_ID")
	  REFERENCES "USERS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IP
--------------------------------------------------------

  ALTER TABLE "IP" ADD CONSTRAINT "FK_IP_BANNER" FOREIGN KEY ("BANNER_ID")
	  REFERENCES "BANNERS" ("ID") ENABLE;
  ALTER TABLE "IP" ADD CONSTRAINT "FK_IP_SITE" FOREIGN KEY ("SITE_ID")
	  REFERENCES "SITES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PAYOUTS
--------------------------------------------------------

  ALTER TABLE "PAYOUTS" ADD CONSTRAINT "FK_PAYOUT_PAYMETHOD" FOREIGN KEY ("PAYMETHOD_ID")
	  REFERENCES "PAYMETHODS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SITES
--------------------------------------------------------

  ALTER TABLE "SITES" ADD CONSTRAINT "FK_SITES_USERS" FOREIGN KEY ("USER_ID")
	  REFERENCES "USERS" ("ID") ENABLE;

    --------------------------------------------------------
--  File created - Sobota-srpen-09-2014   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Trigger BANNERS_ID_AI_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "BANNERS_ID_AI_TRIGGER" 
BEFORE INSERT ON banners 
FOR EACH ROW
 WHEN (new.ID IS NULL) BEGIN
  SELECT banners_seq.NEXTVAL
  INTO   :new.ID
  FROM   dual;
END;
/
ALTER TRIGGER "BANNERS_ID_AI_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger IP_ID_AI_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "IP_ID_AI_TRIGGER" 
BEFORE INSERT ON IP 
FOR EACH ROW
 WHEN (new.ID IS NULL) BEGIN
  SELECT IP_SEQ.NEXTVAL
  INTO   :new.ID
  FROM   dual;
END;
/
ALTER TRIGGER "IP_ID_AI_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PAYMETHODS_ID_AI_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PAYMETHODS_ID_AI_TRIGGER" 
BEFORE INSERT ON paymethods
FOR EACH ROW
 WHEN (new.ID IS NULL) BEGIN
  SELECT paymethods_SEQ.NEXTVAL
  INTO   :new.ID
  FROM   dual;
END;
/
ALTER TRIGGER "PAYMETHODS_ID_AI_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PAYOUTS_ID_AI_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PAYOUTS_ID_AI_TRIGGER" 
BEFORE INSERT ON payouts
FOR EACH ROW
 WHEN (new.ID IS NULL) BEGIN
  SELECT payouts_SEQ.NEXTVAL
  INTO   :new.ID
  FROM   dual;
END;
/
ALTER TRIGGER "PAYOUTS_ID_AI_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SITES_ID_AI_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "SITES_ID_AI_TRIGGER" 
BEFORE INSERT ON sites
FOR EACH ROW
 WHEN (new.ID IS NULL) BEGIN
  SELECT sites_SEQ.NEXTVAL
  INTO   :new.ID
  FROM   dual;
END;
/
ALTER TRIGGER "SITES_ID_AI_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRIGGER_PAYOUT_POSSIBLE
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "TRIGGER_PAYOUT_POSSIBLE" 
BEFORE insert 
ON payouts
FOR EACH ROW
  BEGIN
    CHECK_PAYOUT(:new.amount,:new.user_id);
  END;
/
ALTER TRIGGER "TRIGGER_PAYOUT_POSSIBLE" ENABLE;

--------------------------------------------------------
--  DDL for Trigger USERS_ID_AI_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "USERS_ID_AI_TRIGGER" 
BEFORE INSERT ON USERS 
FOR EACH ROW
 WHEN (new.ID IS NULL) BEGIN
  SELECT USERS_SEQ.NEXTVAL
  INTO   :new.ID
  FROM   dual;
END;
/
ALTER TRIGGER "USERS_ID_AI_TRIGGER" ENABLE;

--------------------------------------------------------
--  DDL for Procedure CHECK_PAYOUT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "CHECK_PAYOUT" 
(
  REQUESTED_AMOUNT IN NUMBER 
, USRID IN NUMBER 
) IS
available_amount number; 
ban number(1,0); 
BEGIN
  
  SELECT cr INTO available_amount FROM (
 SELECT credits cr FROM Credits WHERE credits.USER_ID = USRID)
 where rownum=1;
 
 select banned into ban from (
    select ban banned from users where users.id = USRID
 )
 where rownum=1;
  /*dbms_output.put_line(available_amount);*/
  IF (ban = 1) then
  raise_application_error(-20002, 'Your account is suspended. You cannot request money.');
 END IF;
 IF (available_amount < requested_amount) then
  raise_application_error(-20001, 'You have not enough credits');
 END IF;

UPDATE CREDITS SET CREDITS=available_amount-REQUESTED_AMOUNT WHERE USER_ID=USRID;
  dbms_output.put_line('Your request will be processed');
END CHECK_PAYOUT;
/ 
