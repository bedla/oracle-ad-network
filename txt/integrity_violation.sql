--BANNERS--
--Should fail. USER_ID cannot be null
INSERT INTO "BANNERS" (APPROVED, URL, TITLE) VALUES ('1', 'http://123.com', 'title');

--Should fail (APPROVED should be 0 or 1)
INSERT INTO "BANNERS" (APPROVED, URL,USER_ID, TITLE) VALUES ('3', 'http://123.com', '500', 'title');


--CREDITS--
--Should fail. USER_ID cannot be null
INSERT INTO "CREDITS" (CREDITS) VALUES ('123');


--IP--
--Should fail. USER_ID cannot be null
INSERT INTO "IP" (IP, SITE_ID) VALUES ('81.81.81.81', '900');

--Should fail. SITE_ID cannot be null
INSERT INTO "IP" (BANNER_ID, IP, CLICKTIME) VALUES ('1', '81.81.81.81', TO_DATE('2014-08-08 18:59:41', 'YYYY-MM-DD HH24:MI:SS'));

--Should fail. BANNER_ID cannot be null
INSERT INTO "IP" (SITE_ID, IP, CLICKTIME) VALUES ('1', '81.81.81.81', TO_DATE('2014-08-08 18:59:41', 'YYYY-MM-DD HH24:MI:SS'));

--Should fail. IP is not correct IP address 
INSERT INTO "IP" (BANNER_ID, IP, CLICKTIME, SITE_ID) VALUES ('1', '81..81.81', TO_DATE('2014-08-08 19:01:46', 'YYYY-MM-DD HH24:MI:SS'), '900');

--Should fail. CLICKTIME cannot be null.
INSERT INTO "IP" (BANNER_ID, IP, SITE_ID) VALUES ('1', '81.81.81.81', '900');

--Should fail. USER_ID cannot be null.
INSERT INTO "IP" (BANNER_ID, IP, CLICKTIME, SITE_ID) VALUES ('1', '81..81.81', TO_DATE('2014-08-08 19:01:46', 'YYYY-MM-DD HH24:MI:SS'), '900');


--PAYOUTS--
--Should fail. User with id 1 is suspended (flag ban), so cannot request money
INSERT INTO "PAYOUTS" (USER_ID,AMOUNT, ACCOUNT, REQUESTDATE, PAYMETHOD_ID) VALUES (1,'100', 'abc@def.gh', TO_DATE('2014-08-08 19:12:21', 'YYYY-MM-DD HH24:MI:SS'), 1);

--Should fail. User with id 2 tried request more credits, than is his current balance (requested 10000, have 9567)
INSERT INTO "PAYOUTS" (USER_ID,AMOUNT, ACCOUNT, REQUESTDATE, PAYMETHOD_ID) VALUES (2,'10000', 'abc@def.gh', TO_DATE('2014-08-08 19:12:21', 'YYYY-MM-DD HH24:MI:SS'), 1);


--SITES--
--Should fail. USER_ID cannot be null.
INSERT INTO "SITES" (APPROVED, URL) VALUES ('1', 'http://abc.cz');

--Should fail. URL cannot be null.
INSERT INTO "SITES" (APPROVED, USER_ID) VALUES ('1', '123');

--Should fail (APPROVED should be 0 or 1)
INSERT INTO "SITES" (APPROVED, USER_ID, URL) VALUES ('3', '123', 'http://abc.cz');


--USERS--
--Should fail. EMAIL cannot be null.
INSERT INTO "USERS" (PASSWORD, FIRSTNAME, REGISTRATIONDATE, BAN, SURNAME) VALUES ('FGB79LGM4TS4JU74X5DK49LQ938CX80B7ZBR67DGF2CP9XV11B1ZB60IQ933SU0', 'Jan', TO_DATE('2014-08-08 20:18:09', 'YYYY-MM-DD HH24:MI:SS'), '0', 'Bednar');

--Should fail. EMAIL jan.bednar@@uhk.cz is not valid email address
INSERT INTO "USERS" (EMAIL, PASSWORD, FIRSTNAME, REGISTRATIONDATE, BAN, SURNAME) VALUES ('jan.bednar@@uhk.cz', 'OSN18FKM9VT9JJ34D2SW06LR718SE74N8NXC91SWC9NX7OU08H4KX53ZH905NS7', 'Jan', TO_DATE('2014-08-08 20:20:30', 'YYYY-MM-DD HH24:MI:SS'), '0', 'Bednar');

--Should fail. EMAIL facilisis.non@vestibulumMauris.edu already exists in database
INSERT INTO "USERS" (EMAIL, PASSWORD, FIRSTNAME, REGISTRATIONDATE, BAN, SURNAME) VALUES ('facilisis.non@vestibulumMauris.edu', 'OSN18FKM9VT9JJ34D2SW06LR718SE74N8NXC91SWC9NX7OU08H4KX53ZH905NS7', 'Jan', TO_DATE('2014-08-08 20:20:30', 'YYYY-MM-DD HH24:MI:SS'), '0', 'Bednar');

--Should fail. PASSWORD cannot be null.
INSERT INTO "USERS" (EMAIL, FIRSTNAME, REGISTRATIONDATE, BAN, SURNAME) VALUES ('jan.bednar@uhk.cz', 'Jan', TO_DATE('2014-08-08 20:20:30', 'YYYY-MM-DD HH24:MI:SS'), '0', 'Bednar');

--Should fail (BAN should be 0 or 1)
INSERT INTO "USERS" (EMAIL, PASSWORD, FIRSTNAME, REGISTRATIONDATE, BAN, SURNAME) VALUES ('jan.bednar@uhk.cz', 'OSN18FKM9VT9JJ34D2SW06LR718SE74N8NXC91SWC9NX7OU08H4KX53ZH905NS7', 'Jan', TO_DATE('2014-08-08 20:20:30', 'YYYY-MM-DD HH24:MI:SS'), '3', 'Bednar');