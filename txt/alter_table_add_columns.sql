alter table banners add clicks number;
alter table sites add clicks number;
--------------------------------------------------------
--  DDL for Procedure UPDATE_CLICKS_BY_BANNER_ID
--------------------------------------------------------

  CREATE OR REPLACE PROCEDURE "UPDATE_CLICKS_BY_BANNER_ID" (bannerid in number) as
banner_clicks number;
begin
 select count(banner_id)+1 into banner_clicks from IP where banner_id = bannerid;
 update banners set clicks=banner_clicks where id = bannerid;
 dbms_output.put_line('Clicks for banner '||bannerid||' updated to '||banner_clicks);
end;

/
--------------------------------------------------------
--  DDL for Procedure UPDATE_CLICKS_BY_SITE_ID
--------------------------------------------------------
  CREATE OR REPLACE PROCEDURE "UPDATE_CLICKS_BY_SITE_ID" (siteid in number) as
site_clicks number;
begin
 select count(site_id)+1 into site_clicks from IP where site_id = siteid;
 update sites set clicks=site_clicks where id = siteid;
 dbms_output.put_line('Clicks for site '||siteid||' updated to '||site_clicks);
end;

/
--------------------------------------------------------
--  DDL for Procedure UPDATE_CLICKS_MANUALLY

  CREATE OR REPLACE PROCEDURE "UPDATE_CLICKS_MANUALLY" AS
BEGIN
update banners b set clicks = (
  select count(banner_id) from IP where banner_id = b.id
  );
  update sites s set clicks = (
  select count(site_id) from IP where site_id = s.id
  );
  
END UPDATE_CLICKS_MANUALLY;

/
--------------------------------------------------------
--  DDL for Trigger UPDATE_CLICKS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "UPDATE_CLICKS_TRIGGER" 
BEFORE insert or update or delete
ON IP
for each row  
BEGIN
  UPDATE_CLICKS_BY_BANNER_ID(:new.banner_id);
  UPDATE_CLICKS_BY_SITE_ID(:new.site_id);
END;
/
ALTER TRIGGER "UPDATE_CLICKS_TRIGGER" ENABLE;
--------------------------------------------------------

BEGIN
  UPDATE_CLICKS_MANUALLY();
END;