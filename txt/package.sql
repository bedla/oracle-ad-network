  CREATE OR REPLACE PACKAGE "BANNER_API" AS 

 PROCEDURE BANNERCLICK 
(
  IN_IP IN VARCHAR2 
, SITE_ID IN NUMBER 
, BANNER_ID IN NUMBER 
);

PROCEDURE GET_BANNER 
(
  IN_IP IN VARCHAR2 
, IMG OUT BLOB, BID OUT NUMBER
);

END BANNER_API;

/
CREATE OR REPLACE PACKAGE BODY "BANNER_API" as

PROCEDURE BANNERCLICK 
(
  IN_IP IN VARCHAR2 
, SITE_ID IN NUMBER 
, BANNER_ID IN NUMBER 
) IS 
count_ip NUMBER := 0;
datediff NUMBER := 0;
approved NUMBER :=0;
BEGIN

SELECT s.approved into approved from sites s where s.id=SITE_ID;
if(approved != 1)then 
raise_application_error(-20003, 'This site is not approved'); end if;
  SELECT min(sysdate - CLICKDATE)--Pokud nebyl nalezen zadny zaznam v tabulce IP, je to v poradku. Funkce min vraci v tomto pripade null
    INTO datediff
    FROM 
    (SELECT IP.CLICKTIME as CLICKDATE
    FROM IP
    WHERE IP.IP = IN_IP AND IP.SITE_ID = SITE_ID AND IP.BANNER_ID = BANNER_ID
    ORDER BY IP.CLICKTIME DESC)
    WHERE rownum=1;
    if (datediff<1 and datediff is not null) then 
    raise_application_error(-20004, 'Only one click is allowed per site, banner and date'); end if;

    INSERT INTO IP(ID,SITE_ID,BANNER_ID,IP,CLICKTIME) VALUES (NULL, site_id, banner_id, IN_IP, sysdate);
    UPDATE CREDITS
    SET credits = credits - 1.2
    WHERE user_id = (SELECT BANNERS.USER_ID FROM BANNERS WHERE BANNERS.ID = BANNER_ID AND BANNERS.APPROVED=1 and (SELECT count(*) FROM sites WHERE sites.id=site_id AND  sites.approved=1)>0);

   UPDATE CREDITS
   SET credits = credits + 1
   WHERE user_id = (SELECT sites.user_id FROM sites WHERE sites.id=site_id AND  sites.approved=1 AND (SELECT count(*) FROM banners WHERE banners.id=banner_id AND banners.approved=1)>0); 

END BANNERCLICK;

PROCEDURE GET_BANNER 
(
  IN_IP IN VARCHAR2 
, IMG OUT BLOB, BID OUT NUMBER
) AS 
BEGIN
SELECT BID,IMG
INTO BID,IMG
FROM(
  SELECT banners.id AS BID, banners.img as IMG FROM banners WHERE banners.id NOT IN (
SELECT banners.id FROM banners
LEFT JOIN ip ON banners.id = ip.banner_id WHERE ip.ip=IN_IP
group by banners.id,ip.ip
having (sysdate - max(ip.clicktime))<1)
AND banners.approved = 1
ORDER BY dbms_random.value)
WHERE rownum=1;

if(BID IS NULL)
then
begin
SELECT BID, IMG 
INTO BID, IMG
FROM(
    SELECT banners.id AS BID, banners.img as IMG FROM banners WHERE banners.approved = 1
    ORDER BY dbms_random.value)
    WHERE rownum=1;
end;
end if;
dbms_output.put_line('BID='||BID);
END GET_BANNER;
END BANNER_API;

/
