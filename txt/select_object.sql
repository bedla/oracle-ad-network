SET AUTOTRACE ON
set echo on
set time on
set timing on
set serveroutput on

--Paymethods stats
select p.paymethod_ref.methodname, sum(p.amount) as amount
from o_payouts p
group by p.paymethod_ref.methodname;
list;

--Unpaid users
select p.user_ref.firstname AS firstname,p.user_ref.surname AS surname,
p.paymethod_ref.methodname AS methodName,p.amount AS amount,p.account AS account,
p.requestDate AS requestDate 
from 
o_payouts p
where p.status = 0 and p.user_ref.ban=0
order by p.requestDate asc;
list;

--Users to contact
select f||' '||s as name, e as email, r as reason from(
  select s.user_ref.id as id, s.user_ref.firstname as f, s.user_ref.surname as s, s.user_ref.email as e, 'User have some unapproved site' as r from o_sites s where s.approved = 0
  UNION 
  select b.user_ref.id as id, b.user_ref.firstname as f, b.user_ref.surname as s, b.user_ref.email as e, 'User have some unapproved banner' as r from o_banners b where b.approved = 0
  UNION
  select u.id as id, u.firstname as f, u.surname as s, u.email as e, 'User is banned' as r from o_users u where u.ban=1
  UNION
  select p.user_ref.id as id, p.user_ref.firstname as f, p.user_ref.surname as s, p.user_ref.email as e, 'User is waiting for payout' as r 
  from o_payouts p
  where p.status=0
  )
  order by id;
list;  

  --Month click stats grouped by banners
  select EXTRACT(month FROM ip.clicktime) as month_number, ip.banner_ref.title as BANNER_TITLE, count(*) as CLICKS from
  O_IP ip 
  where (ip.clicktime >= SYSDATE-365) 
  group by EXTRACT(month FROM ip.clicktime),ip.banner_ref.title;
  list;

  -- Users withdrawal stats
  SELECT (SELECT firstname||' '||surname as name from o_users where id=p.user_ref.id) as name,
min(p.requestdate) as first_request, max (p.requestdate) as last_request, count(*) as count_requests, 
SUM(p.AMOUNT) as total_amount FROM
O_PAYOUTS p
where p.status = 1
GROUP BY p.user_ref.id
ORDER by SUM(p.AMOUNT) desc;
list;