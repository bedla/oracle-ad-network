Insert into O_USERS (ID,EMAIL,PASSWORD,FIRSTNAME,REGISTRATIONDATE,BAN,SURNAME)
    SELECT
       USERS.ID AS ID,
       USERS.EMAIL AS EMAIL,
       USERS.PASSWORD AS PASSWORD,
       USERS.FIRSTNAME AS FIRSTNAME,
       USERS.REGISTRATIONDATE AS REGISTRATIONDATE,
       USERS.BAN AS BAN,
       USERS.SURNAME AS SURNAME
    FROM USERS;
    
Insert into O_SITES (ID,APPROVED,USER_REF,URL)
SELECT
s.ID AS ID,
s.APPROVED AS APPROVED,
REF(u) AS USER_REF,
s.URL AS URL
FROM SITES s left join O_USERS u on u.ID = s.USER_ID;

Insert into o_paymethods (id,methodname)
select
p.id as ID,
p.methodname as methodname
from
paymethods p;

Insert into O_PAYOUTS (ID,USER_REF,AMOUNT,ACCOUNT,REQUESTDATE,PAYMETHOD_REF,STATUS)
select
p.id as ID,
ref(u) as USER_REF,
p.amount as AMOUNT,
p.account as ACCOUNT,
p.requestdate as REQUESTDATE,
ref(m) as PAYMETHOD_REF,
p.status as STATUS
from
PAYOUTS p 
left join O_USERS u on u.ID = p.USER_ID
left join o_paymethods m on m.id = p.paymethod_id;

Insert into O_BANNERS (ID,APPROVED,IMG,URL,TITLE,USER_REF)
select
b.id,
b.approved,
b.img,
b.url,
b.title,
ref(u)
from
banners b left join o_users u on b.user_id=u.id;


Insert into O_CREDITS (USER_REF,CREDITS)
select
ref(u),c.credits
from
credits c left join o_users u on c.user_id=u.id;

Insert into O_IP (ID,BANNER_REF,IP,CLICKTIME,SITE_REF)
select
ip.id,ref(b),ip.ip,ip.clicktime,ref(s)
from ip 
left join o_banners b on b.id=ip.banner_id
left join o_sites s on s.id=ip.site_id;
