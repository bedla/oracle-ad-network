--------------------------------------------------------
--  File created - Ned�le-srpen-10-2014   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Type T_USER
--------------------------------------------------------

  CREATE OR REPLACE TYPE "T_USER" as object
   (	"ID" NUMBER(12,0), 
	"EMAIL" VARCHAR2(255 BYTE), 
	"PASSWORD" CHAR(64 BYTE), 
	"FIRSTNAME" VARCHAR2(30 BYTE), 
	"REGISTRATIONDATE" DATE , 
	"BAN" NUMBER(1,0), 
	"SURNAME" VARCHAR2(50 BYTE)); 

/
--------------------------------------------------------
--  DDL for Type T_BANNER
--------------------------------------------------------

  CREATE OR REPLACE TYPE "T_BANNER" as object
   (	"ID" NUMBER(12,0), 
	"APPROVED" NUMBER(1,0), 
	"IMG" BLOB, 
	"URL" VARCHAR2(1024 BYTE), 
	"TITLE" VARCHAR2(50 BYTE), 
	"USER_REF" ref t_user)

/
--------------------------------------------------------
--  DDL for Type T_CREDIT
--------------------------------------------------------

  CREATE OR REPLACE TYPE "T_CREDIT" as object
   (	"USER_REF" ref t_user, 
	"CREDITS" NUMBER(10,4)) 

/

--------------------------------------------------------
--  DDL for Type T_SITE
--------------------------------------------------------

  CREATE OR REPLACE TYPE "T_SITE" as object
   (	"ID" NUMBER(12,0), 
	"APPROVED" NUMBER(1,0), 
	"USER_REF" ref t_user , 
	"URL" VARCHAR2(1024 BYTE))

/
--------------------------------------------------------
--  DDL for Type T_IP
--------------------------------------------------------

  CREATE OR REPLACE TYPE "T_IP" as object
   (	"ID" NUMBER(12,0), 
	"BANNER_REF" ref t_banner, 
	"IP" VARCHAR2(45 BYTE), 
	"CLICKTIME" DATE, 
	"SITE_REF" ref t_site)

/
--------------------------------------------------------
--  DDL for Type T_PAYMETHOD
--------------------------------------------------------

  CREATE OR REPLACE TYPE "T_PAYMETHOD" as object 
   (	"ID" NUMBER(12,0), 
	"METHODNAME" VARCHAR2(20 BYTE))

/
--------------------------------------------------------
--  DDL for Type T_PAYOUT
--------------------------------------------------------

  CREATE OR REPLACE TYPE "T_PAYOUT" as object 
   (	"ID" NUMBER(12,0), 
	"USER_REF" ref t_user, 
	"AMOUNT" NUMBER(10,2), 
	"ACCOUNT" VARCHAR2(255 BYTE), 
	"REQUESTDATE" DATE, 
	"PAYMETHOD_REF" ref t_paymethod, 
	"STATUS" NUMBER(1,0)) 

/

--------------------------------------------------------
--  DDL for Table O_USERS
--------------------------------------------------------

  CREATE TABLE "O_USERS" OF "T_USER" 
 ;
--------------------------------------------------------
--  DDL for Table O_BANNERS
--------------------------------------------------------

  CREATE TABLE "O_BANNERS" OF "T_BANNER" 
   (	SCOPE FOR ("USER_REF") IS "O_USERS" 
   ) ;

--------------------------------------------------------
--  DDL for Table O_SITES
--------------------------------------------------------

  CREATE TABLE "O_SITES" OF "T_SITE" 
   (	SCOPE FOR ("USER_REF") IS "O_USERS" 
   ) ;   
--------------------------------------------------------
--  DDL for Table O_CREDITS
--------------------------------------------------------

  CREATE TABLE "O_CREDITS" OF "T_CREDIT" 
   (	SCOPE FOR ("USER_REF") IS "O_USERS" 
   ) ;
--------------------------------------------------------
--  DDL for Table O_IP
--------------------------------------------------------

  CREATE TABLE "O_IP" OF "T_IP" 
   (	SCOPE FOR ("BANNER_REF") IS "O_BANNERS" , 
	SCOPE FOR ("SITE_REF") IS "O_SITES" 
   ) ;
--------------------------------------------------------
--  DDL for Table O_PAYMETHODS
--------------------------------------------------------

  CREATE TABLE "O_PAYMETHODS" OF "T_PAYMETHOD" 
 ;
--------------------------------------------------------
--  DDL for Table O_PAYOUTS
--------------------------------------------------------

  CREATE TABLE "O_PAYOUTS" OF "T_PAYOUT" 
   (	SCOPE FOR ("USER_REF") IS "O_USERS" , 
	SCOPE FOR ("PAYMETHOD_REF") IS "O_PAYMETHODS" 
   ) ;

--------------------------------------------------------
--  Ref Constraints for Table O_BANNERS
--------------------------------------------------------

  ALTER TABLE "O_BANNERS" ADD FOREIGN KEY ("USER_REF")
	  REFERENCES "O_USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table O_CREDITS
--------------------------------------------------------

  ALTER TABLE "O_CREDITS" ADD FOREIGN KEY ("USER_REF")
	  REFERENCES "O_USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table O_IP
--------------------------------------------------------

  ALTER TABLE "O_IP" ADD FOREIGN KEY ("BANNER_REF")
	  REFERENCES "O_BANNERS"  ENABLE;
  ALTER TABLE "O_IP" ADD FOREIGN KEY ("SITE_REF")
	  REFERENCES "O_SITES"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table O_PAYOUTS
--------------------------------------------------------

  ALTER TABLE "O_PAYOUTS" ADD FOREIGN KEY ("USER_REF")
	  REFERENCES "O_USERS"  ENABLE;
  ALTER TABLE "O_PAYOUTS" ADD FOREIGN KEY ("PAYMETHOD_REF")
	  REFERENCES "O_PAYMETHODS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table O_SITES
--------------------------------------------------------

  ALTER TABLE "O_SITES" ADD FOREIGN KEY ("USER_REF")
	  REFERENCES "O_USERS"  ENABLE;
