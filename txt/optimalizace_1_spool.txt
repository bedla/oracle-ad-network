> set AUTOTRACE ON
Autotrace Enabled
Shows the execution plan as well as statistics of the statement.
> set time on
line 3: SQLPLUS Command Skipped: set time on
> set timing on
> set serveroutput on
> --predchozi
Unable to gather statistics please unsure user has correct access.
The statistic feature requires that the user is granted select on v_$sesstat, v_$statname and v_$session.
> select methodname, amount from(
select p.paymethod_id as mid, sum(p.amount) as amount
from payouts p
group by p.paymethod_id)
left join paymethods m on mid=m.id
METHODNAME               AMOUNT
-------------------- ----------
Paypal                   227287 
Paysec                   228045 
Alertpay                 114158 
Moneybookers             198451 

Elapsed: 00:00:02.277
Plan hash value: 4269656994
 
-----------------------------------------------------------------------------------
| Id  | Operation            | Name       | Rows  | Bytes | Cost (%CPU)| Time     |
-----------------------------------------------------------------------------------
|   0 | SELECT STATEMENT     |            |     4 |   152 |     8  (25)| 00:00:01 |
|*  1 |  HASH JOIN OUTER     |            |     4 |   152 |     8  (25)| 00:00:01 |
|   2 |   VIEW               |            |     4 |   104 |     4  (25)| 00:00:01 |
|   3 |    HASH GROUP BY     |            |     4 |    28 |     4  (25)| 00:00:01 |
|   4 |     TABLE ACCESS FULL| PAYOUTS    |   229 |  1603 |     3   (0)| 00:00:01 |
|   5 |   TABLE ACCESS FULL  | PAYMETHODS |     4 |    48 |     3   (0)| 00:00:01 |
-----------------------------------------------------------------------------------
 
Predicate Information (identified by operation id):
---------------------------------------------------
 
   1 - access("MID"="M"."ID"(+))

Unable to gather statistics please unsure user has correct access.
The statistic feature requires that the user is granted select on v_$sesstat, v_$statname and v_$session.
> --optimalizovany
Unable to gather statistics please unsure user has correct access.
The statistic feature requires that the user is granted select on v_$sesstat, v_$statname and v_$session.
> select (select methodname from paymethods where id=p.paymethod_id), sum(p.amount) as amount
from payouts p
group by p.paymethod_id
(SELECTMETHODNAMEFROMPAYMETHODSWHEREID=P.PAYMETHOD_ID)     AMOUNT
------------------------------------------------------ ----------
Paypal                                                     227287 
Paysec                                                     228045 
Moneybookers                                               198451 
Alertpay                                                   114158 

Elapsed: 00:00:00.100
Plan hash value: 4100091249
 
--------------------------------------------------------------------------------------------
| Id  | Operation                   | Name         | Rows  | Bytes | Cost (%CPU)| Time     |
--------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT            |              |     4 |    28 |     4  (25)| 00:00:01 |
|   1 |  TABLE ACCESS BY INDEX ROWID| PAYMETHODS   |     1 |    12 |     1   (0)| 00:00:01 |
|*  2 |   INDEX UNIQUE SCAN         | PK_PAYMETHOD |     1 |       |     0   (0)| 00:00:01 |
|   3 |  HASH GROUP BY              |              |     4 |    28 |     4  (25)| 00:00:01 |
|   4 |   TABLE ACCESS FULL         | PAYOUTS      |   229 |  1603 |     3   (0)| 00:00:01 |
--------------------------------------------------------------------------------------------
 
Predicate Information (identified by operation id):
---------------------------------------------------
 
   2 - access("ID"=:B1)

Unable to gather statistics please unsure user has correct access.
The statistic feature requires that the user is granted select on v_$sesstat, v_$statname and v_$session.
