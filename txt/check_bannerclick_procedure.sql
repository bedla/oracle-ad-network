set serveroutput on;
DECLARE
  IN_IP VARCHAR2(200);
  SITE_ID NUMBER;
  BANNER_ID NUMBER;
  BANNER_USER_CREDITS_BEFORE NUMBER;
  BANNER_USER_CREDITS_AFTER NUMBER;
  SITE_USER_CREDITS_BEFORE NUMBER;
  SITE_USER_CREDITS_AFTER NUMBER;
BEGIN
  IN_IP := '29.44.57.68';
  SITE_ID := 601;
  BANNER_ID := 2;

   SELECT credits
   INTO SITE_USER_CREDITS_BEFORE
   FROM CREDITS
   WHERE user_id = (SELECT sites.user_id FROM sites WHERE sites.id=SITE_ID); 

  dbms_output.put_line('SITE OWNER CREDITS BEFORE CLICK='||SITE_USER_CREDITS_BEFORE);
  
   SELECT credits
   INTO BANNER_USER_CREDITS_BEFORE
   FROM credits
   WHERE user_id = (SELECT banners.user_id FROM banners WHERE banners.id=banner_id); 

  dbms_output.put_line('BANNER OWNER CREDITS BEFORE CLICK='||SITE_USER_CREDITS_BEFORE);
  BANNER_API.BANNERCLICK(
    IN_IP => IN_IP,
    SITE_ID => SITE_ID,
    BANNER_ID => BANNER_ID
  );
  SELECT credits
   INTO SITE_USER_CREDITS_AFTER
   FROM credits
   WHERE user_id = (SELECT sites.user_id FROM sites WHERE sites.id=site_id AND  sites.approved=1); 

  dbms_output.put_line('SITE OWNER CREDITS BEFORE CLICK (SHOULD BE +1)='||SITE_USER_CREDITS_AFTER);
  
   SELECT credits
   INTO BANNER_USER_CREDITS_AFTER
   FROM credits
   WHERE user_id = (SELECT banners.user_id FROM banners WHERE banners.id=banner_id AND  banners.approved=1); 

  dbms_output.put_line('BANNER OWNER CREDITS AFTER CLICK (SHOULD BE -1,2)='||BANNER_USER_CREDITS_AFTER);
  
  
  dbms_output.put_line('RUNNING PROCEDURE AGAIN WITH SAME VALUES. SHOULD THROW EXCEPTION');
  BANNER_API.BANNERCLICK(
    IN_IP => IN_IP,
    SITE_ID => SITE_ID,
    BANNER_ID => BANNER_ID
  );
rollback;
  
END;